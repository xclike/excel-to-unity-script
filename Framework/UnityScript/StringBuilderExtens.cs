﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExcelToUnityScript.UnityScript
{
    public static class StringBuilderExtens
    {
        public static StringBuilder AppendN(this StringBuilder bui, string value)
        {
            return bui.Append(value).Append("\r\n");
        }

        public static StringBuilder AppendTN(this StringBuilder bui, string value, int t = 1)
        {
            for (int i = 0; i < t; i++)
                bui.Append("\t");

            return bui.Append(value).Append("\r\n");
        }
        public static StringBuilder AppendT(this StringBuilder bui, string value, int t = 1)
        {
            for (int i = 0; i < t; i++)
                bui.Append("\t");

            return bui.Append(value);
        }
    }
}
