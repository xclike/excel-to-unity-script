﻿using CommandLine;

using Excel;

using ExcelToUnityScript;
using ExcelToUnityScript.UnityScript;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace excelToUnityScript
{
    class Program
    {
        static void Main(string[] args)
        {

            if (null == args || args.Length == 0)
            {

#if DEBUG
                args = new string[] { "-f", " E:/Unity/ArmedRescue/Project/Assets/Resources/Config/Attr.xlsx",
                    "-o", "E:/Unity/ArmedRescue/Project/Assets/Script/CSVCfg/" };
#else
return;
#endif
            }

            Parser.Default.ParseArguments<Options>(args).WithParsed(Run);
        }

        private static void Run(Options obj)
        {

            string inFile = obj.InFile;
            string outPath = obj.OutPath;

            string inheritClass = obj.inheritClass;
            string inheritClassData = obj.inheritClassData;


            if (string.IsNullOrEmpty(inheritClass))
                inheritClass = "CSVBaseConfig";

            if (!File.Exists(inFile))
                throw new Exception($"{inFile}  文件不存在");


            if (!Directory.Exists(outPath))
                throw new Exception($"{outPath} 输出路径不存在");


            var fileName = Path.GetFileNameWithoutExtension(inFile);

            ExcelHelper excel = new ExcelHelper(obj.InFile.ToString());
            var sheet = excel.GetSheet(0);
            var lines = excel.ExportToCVSData(sheet);
            CSVScript scripte = new CSVScript(obj.IgnoreNum, obj.IsBackUp);

            string path = scripte.CreateCSVConfigFile(lines, fileName, outPath, inheritClass, inheritClassData);
            Console.WriteLine(path);

        }
    }
}
