# Excel 转 unity 配置脚本
# .net core 3.1

[博客地址](https://blog.csdn.net/qq_29124109/article/details/120081285)

#### 介绍

| 数据类型 | 表格   |
|---------|--------|
| String  | string 
| Double  | double |
| Float   | float  |
| Int     | int    |
| Vector2 | v2     |
| Vector3 | v3     |
| Bool    | bool   |

excel 转 unity 配置脚本

![Excel 文件](https://gitee.com/xclike/excel-to-unity-script/raw/master/.net%20core/image/image1.png "Excel 文件")
![生成的配置文件](https://gitee.com/xclike/excel-to-unity-script/raw/master/.net%20core/image/image2.png "生成的配置文件")
![生成的配置文件](https://gitee.com/xclike/excel-to-unity-script/raw/master/.net%20core/image/image3.png "生成的配置文件")
![参数](https://gitee.com/xclike/excel-to-unity-script/raw/master/.net%20core/image/image4.png "参数")