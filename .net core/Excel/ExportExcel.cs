﻿//using NPOI.HSSF.UserModel;
//using NPOI.HSSF.Util;
//using NPOI.SS.UserModel;
//using NPOI.XSSF.UserModel;
//using System;
//using System.Collections.Generic;
//using System.Data;
//using System.Drawing;
//using System.IO;
//using System.Linq;
//using YZJRobot.DbTable;
//using YZJRobot.Error;
//using YZJRobot.Tool;

//namespace YZJRobot.Excel
//{
//    /// <summary>
//    /// 导出excel 
//    /// </summary>
//    public class ExportExcel
//    {

//        private static IWorkbook GetWorkBook(string name = "日志模版")
//        {
//            Stream stream = null;
//            try
//            {
//                stream = Util.ReleaseFile(name);
//                if (stream != null)
//                    return new XSSFWorkbook(stream);
//            }
//            catch (Exception e)
//            {
//                try
//                {
//                    Console.WriteLine(e.Message);
//                    if (stream != null)
//                        return new HSSFWorkbook(stream);

//                }
//                catch (Exception e2)
//                {
//                    Console.WriteLine(e2.Message);
//                }
//            }
//            return null;
//        }

//        public static ICellStyle GetStyle(IWorkbook workBook, Color color)
//        {
//            ICellStyle style = workBook.CreateCellStyle();
//            style.BorderBottom = BorderStyle.Thin;
//            style.BorderLeft = BorderStyle.Thin;
//            style.BorderRight = BorderStyle.Thin;
//            style.BorderTop = BorderStyle.Thin;

//            style.FillPattern = FillPattern.SolidForeground;

//            short borderColor = 0;
//            if (style is XSSFCellStyle)
//            {
//                ((XSSFCellStyle)style).SetFillForegroundColor(GetColor(color));

//                borderColor = GetColor(Color.FromArgb(0x00, 0x00, 0x00)).Indexed;
//            }
//            else if (style is HSSFCellStyle)
//            {
//                ((HSSFCellStyle)style).FillForegroundColor = GetColor(workBook, color);
//                borderColor = GetColor(workBook, Color.FromArgb(0x00, 0x00, 0x00));
//            }




//            style.TopBorderColor = borderColor;
//            style.BottomBorderColor = borderColor;
//            style.LeftBorderColor = borderColor;
//            style.RightBorderColor = borderColor;
//            return style;
//        }

//        public static XSSFColor GetColor(Color color)
//        {
//            return new XSSFColor(color);
//        }
//        static short LastColorIndex = 0x08;
//        public static short GetColor(IWorkbook workBook, Color color)
//        {
//            var hsbook = ((HSSFWorkbook)workBook);
//            HSSFPalette palette = hsbook.GetCustomPalette();
//            HSSFColor hssf = palette.FindColor(color.R, color.G, color.B);
//            if (hssf == null)
//            {
//                palette.SetColorAtIndex(LastColorIndex, color.R, color.G, color.B);
//                hssf = palette.GetColor(LastColorIndex);
//                LastColorIndex++;
//            }
//            return hssf.Indexed;
//        }


//        public static bool WriteScore(string filePath)
//        {
//            IWorkbook workBook = GetWorkBook("打分模版");
//            try
//            {
//                SaveFile(filePath, workBook);
//                return true;
//            }
//            catch
//            {
//                return false;
//            }

//        }
//        /// <summary>
//        /// 写出月工作计划
//        /// </summary>
//        /// <param name="plans"></param>
//        /// <param name="filePath"></param>
//        public static bool WriteMonthWorkPlan(DataTable plans, string filePath)
//        {
//            IWorkbook workBook;

//            if (plans != null)
//                workBook = GetWorkBook("月计划模板");
//            else
//                workBook = GetWorkBook("月计划模板新");

//            if (workBook == null)
//            {
//                MessageX.Show("月计划模板打开失败");
//                return false;
//            }

//            var sheet = workBook.GetSheetAt(0);
//            if (sheet == null)
//            {
//                MessageX.Show("工作薄打开失败");
//                workBook.Close();
//                return false;
//            }
//            try
//            {
//                var work = Export(workBook, sheet, plans);
//                SaveFile(filePath, work);

//                return true;
//            }
//            catch (Exception ex)
//            {
//                YzjError.Error("导出失败" + ex.ErrorInfo());

//            }
//            return false;
//        }

//        public static void CreateHead(IWorkbook objwork, ISheet sheet, DataTable table)
//        {
//            List<string> names = new List<string>();
//            for (int i = 0; i < table.Columns.Count; i++)
//                names.Add(table.Columns[i].ColumnName);
//            CreateHead(objwork, sheet, names);
//        }

//        public static void CreateHead(IWorkbook objwork, ISheet sheet, List<string> table, Color? headClolr = null)
//        {
//            if (null == headClolr)
//                headClolr = Color.FromArgb(244, 176, 132);

//            ICellStyle style = GetStyle(objwork, headClolr.Value);
//            IFont font = objwork.CreateFont();
//            font.Color = GetColor(objwork, Color.FromArgb(0x00, 0x00, 0x00));
//            font.FontHeightInPoints = 11;
//            font.FontName = "宋体";
//            style.VerticalAlignment = VerticalAlignment.Center;
//            style.Alignment = HorizontalAlignment.Center;
//            style.SetFont(font);

//            var row = sheet.GetRow(0);
//            if (row == null)
//                row = sheet.CreateRow(0);

//            row.Height = 2 * 256;
//            for (int i = 0; i < table.Count; i++)
//            {
//                AddCell(row, i, table[i], style);
//            }
//        }

//        public static object Export(IWorkbook objwork, ISheet sheet, DataTable table)
//        {
//            if (table != null)
//            {
//                LastColorIndex = 0x08;

//                CreateHead(objwork, sheet, table);

//                ICellStyle style = GetStyle(objwork, Color.FromArgb(169, 208, 142));
//                IFont font = objwork.CreateFont();
//                font.Color = GetColor(objwork, Color.FromArgb(0x00, 0x00, 0x00));
//                font.FontHeightInPoints = 11;
//                font.FontName = "宋体";

//                style.VerticalAlignment = VerticalAlignment.Center;
//                style.Alignment = HorizontalAlignment.Center;
//                style.SetFont(font);


//                style.DataFormat = objwork.CreateDataFormat().GetFormat("0.00");

//                for (int i = 0; i < table.Rows.Count; i++)
//                {
//                    var tablerow = table.Rows[i];
//                    IRow row = sheet.CreateRow(i + 1);
//                    for (int j = 0; j < tablerow.ItemArray.Length; j++)
//                    {
//                        AddCell(row, j, tablerow[j], style);
//                    }
//                    row.Height = 2 * 256;
//                }


//                AutoColumnWidth(sheet, table.Columns.Count + 1);

//            }
//            return objwork;
//        }

//        private static void AutoColumnWidth(ISheet sheet, int cols)
//        {
//            for (int col = 0; col <= cols; col++)
//            {
//                sheet.AutoSizeColumn(col);//自适应宽度，但是其实还是比实际文本要宽
//            }
//        }

//        public static void SaveFile(string filepath, object obj)
//        {
//            var book = (IWorkbook)obj;
//            using (MemoryStream ms = new MemoryStream())
//            {
//                book.Write(ms);
//                using (FileStream fs = new FileStream(filepath, FileMode.Create, FileAccess.Write))
//                {
//                    byte[] data = ms.ToArray();
//                    fs.Write(data, 0, data.Length);
//                    fs.Flush();
//                }
//                book = null;
//            }
//        }

//        private static void AddCell(IRow row, int index, object data, ICellStyle style)
//        {
//            if (null == data)
//                data = "";

//            Type type = data.GetType();
//            if (type == typeof(DateTime))
//            {
//                AddCellDatetime(row, index, (DateTime)data, style);
//            }
//            else if (type == typeof(bool))
//            {
//                AddCellBool(row, index, (bool)data, style);
//            }
//            else if (double.TryParse(data.ToString(), out double reult))
//            {
//                //var dataFromat = style.DataFormat;

//                AddCellNumber(row, index, reult, style);
//                //style.DataFormat = dataFromat;
//            }
//            else
//            {
//                AddCellString(row, index, data.ToString(), style);
//            }
//        }

//        private static void AddCellDatetime(IRow row, int index, DateTime value, ICellStyle style)
//        {
//            if (value.Year == 1 && value.Month == 1 && value.Day == 1)
//            {
//                var cell = row.CreateCell(index);
//                cell.CellStyle = style;
//            }
//            else
//            {
//                var cell = row.CreateCell(index);
//                cell.SetCellValue(value.ToString("yyyy-MM-dd"));
//                cell.CellStyle = style;
//            }

//        }
//        private static void AddCellNumber(IRow row, int index, double value, ICellStyle style)
//        {
//            var cell = row.CreateCell(index, CellType.Numeric);
//            cell.SetCellValue(value);
//            cell.CellStyle = style;
//        }
//        private static void AddCellBool(IRow row, int index, bool value, ICellStyle style)
//        {
//            var cell = row.CreateCell(index);
//            cell.SetCellValue(value ? "是" : "否");
//            cell.CellStyle = style;
//        }
//        private static void AddCellString(IRow row, int index, string value, ICellStyle style)
//        {
//            var cell = row.CreateCell(index);
//            cell.SetCellValue(value);
//            cell.CellStyle = style;
//        }


//        public static void ExportPlan(List<TodayOverTask> plans, string fileName)
//        {
//            IWorkbook objwork = new HSSFWorkbook();
//            ISheet sheet = objwork.CreateSheet("交接时间");


//            LastColorIndex = 0x08;

//            var names = plans[0].GetNames();
//            CreateHead(objwork, sheet, names, Color.FromArgb(113, 113, 113));


//            ICellStyle style1 = GetStyle(objwork, Color.FromArgb(174, 174, 174));
//            ICellStyle style2 = GetStyle(objwork, Color.FromArgb(189, 189, 189));
//            ICellStyle style3 = GetStyle(objwork, Color.FromArgb(217, 217, 217));
//            ICellStyle style4 = GetStyle(objwork, Color.FromArgb(227, 227, 227));

//            IFont font = objwork.CreateFont();
//            font.Color = GetColor(objwork, Color.FromArgb(0x00, 0x00, 0x00));
//            font.FontHeightInPoints = 11;
//            font.FontName = "宋体";

//            IFont fontError = objwork.CreateFont();
//            fontError.Color = GetColor(objwork, Color.FromArgb(0xff, 0x00, 0x00));
//            fontError.FontHeightInPoints = 11;
//            fontError.FontName = "宋体";

//            style1.VerticalAlignment = VerticalAlignment.Center;
//            style1.Alignment = HorizontalAlignment.Center;
//            style1.SetFont(font);

//            style2.VerticalAlignment = VerticalAlignment.Center;
//            style2.Alignment = HorizontalAlignment.Center;
//            style2.SetFont(font);

//            style3.VerticalAlignment = VerticalAlignment.Center;
//            style3.Alignment = HorizontalAlignment.Center;
//            style3.SetFont(font);

//            style4.VerticalAlignment = VerticalAlignment.Center;
//            style4.Alignment = HorizontalAlignment.Center;
//            style4.SetFont(font);

//            List<TodayMonthPlan> sortPlan = new List<TodayMonthPlan>();
//            var group = plans.GroupBy(na => na.Group);

//            int index = 1;
//            foreach (var item_group in group)
//            {
//                var png = item_group.GroupBy(na => na.EName);
//                foreach (var item_png in png)
//                {
//                    ICellStyle userSytle = style1;
//                    Console.WriteLine(item_png.FirstOrDefault().Group);
//                    switch (item_png.FirstOrDefault().Group)
//                    {
//                        case "交互组":
//                            userSytle = style4;
//                            break;
//                        case "后期组":
//                            userSytle = style3;
//                            break;
//                        case "项目组":
//                            userSytle = style2;
//                            break;
//                        case "米格组":
//                            userSytle = style1;
//                            break;
//                    }
//                    userSytle.SetFont(font);
//                    foreach (var item in item_png)
//                    {
//                        ICellStyle cache = userSytle;
//                        IRow row = sheet.CreateRow(index++);
//                        row.Height = 2 * 256;
//                        var value = item.GetValues();

//                        for (int j = 0; j < value.Count; j++)
//                        {
//                            if (j == value.Count - 1)
//                            {
//                                if (item.OverTime < DateTime.Now)
//                                {
//                                    ICellStyle errorStyle = GetStyle(objwork, Color.FromArgb(0, 0, 0));
//                                    errorStyle.CloneStyleFrom(userSytle);
//                                    errorStyle.SetFont(fontError);
//                                    cache = errorStyle;
//                                }
//                            }

//                            AddCell(row, j, value[j], cache);
//                        }
//                    }
//                }
//            }

//            AutoColumnWidth(sheet, names.Count);
//            SaveFile(fileName, objwork);
//        }


//        public static void ExportPlan(List<TodayMonthPlan> plans, string fileName)
//        {
//            IWorkbook objwork = new HSSFWorkbook();
//            ISheet sheet = objwork.CreateSheet("交接时间");


//            LastColorIndex = 0x08;

//            var names = plans[0].GetNames();
//            CreateHead(objwork, sheet, names, Color.FromArgb(113, 113, 113));


//            ICellStyle style1 = GetStyle(objwork, Color.FromArgb(174, 174, 174));
//            ICellStyle style2 = GetStyle(objwork, Color.FromArgb(189, 189, 189));
//            ICellStyle style3 = GetStyle(objwork, Color.FromArgb(217, 217, 217));
//            ICellStyle style4 = GetStyle(objwork, Color.FromArgb(227, 227, 227));

//            IFont font = objwork.CreateFont();
//            font.Color = GetColor(objwork, Color.FromArgb(0x00, 0x00, 0x00));
//            font.FontHeightInPoints = 11;
//            font.FontName = "宋体";

//            IFont fontError = objwork.CreateFont();
//            fontError.Color = GetColor(objwork, Color.FromArgb(0xff, 0x00, 0x00));
//            fontError.FontHeightInPoints = 11;
//            fontError.FontName = "宋体";

//            style1.VerticalAlignment = VerticalAlignment.Center;
//            style1.Alignment = HorizontalAlignment.Center;
//            style1.SetFont(font);

//            style2.VerticalAlignment = VerticalAlignment.Center;
//            style2.Alignment = HorizontalAlignment.Center;
//            style2.SetFont(font);

//            style3.VerticalAlignment = VerticalAlignment.Center;
//            style3.Alignment = HorizontalAlignment.Center;
//            style3.SetFont(font);

//            style4.VerticalAlignment = VerticalAlignment.Center;
//            style4.Alignment = HorizontalAlignment.Center;
//            style4.SetFont(font);

//            List<TodayMonthPlan> sortPlan = new List<TodayMonthPlan>();
//            var group = plans.GroupBy(na => na.currentGroup);

//            int index = 1;
//            foreach (var item_group in group)
//            {
//                var png = item_group.GroupBy(na => na.ename);
//                foreach (var item_png in png)
//                {
//                    ICellStyle userSytle = style1;
//                    Console.WriteLine(item_png.FirstOrDefault().currentGroup);
//                    switch (item_png.FirstOrDefault().currentGroup)
//                    {
//                        case "交互组":
//                            userSytle = style4;
//                            break;
//                        case "后期组":
//                            userSytle = style3;
//                            break;
//                        case "项目组":
//                            userSytle = style2;
//                            break;
//                        case "米格组":
//                            userSytle = style1;
//                            break;
//                    }
//                    userSytle.SetFont(font);
//                    foreach (var item in item_png)
//                    {
//                        ICellStyle cache = userSytle;
//                        IRow row = sheet.CreateRow(index++);
//                        row.Height = 2 * 256;
//                        var value = item.GetValues();

//                        for (int j = 0; j < value.Count; j++)
//                        {
//                            if (j == value.Count - 1)
//                            {
//                                if (item.isOverdur)
//                                {
//                                    ICellStyle errorStyle = GetStyle(objwork, Color.FromArgb(0, 0, 0));
//                                    errorStyle.CloneStyleFrom(userSytle);
//                                    errorStyle.SetFont(fontError);
//                                    cache = errorStyle;
//                                }
//                            }

//                            AddCell(row, j, value[j], cache);
//                        }
//                    }
//                }
//            }

//            AutoColumnWidth(sheet, names.Count);
//            SaveFile(fileName, objwork);
//        }

//        public static void Export(DataTable table, string fileName, string sheetName)
//        {

//            IWorkbook workBook = new HSSFWorkbook();
//            ISheet sheet = workBook.CreateSheet(sheetName);
//            var obj = Export(workBook, sheet, table);
//            SaveFile(fileName, obj);
//            RunProcess.OpenFileDir(fileName);
//        }

//        public static void Write(LogEntity[] dailys, string fileName)
//        {
//            var workBook = GetWorkBook();
//            if (workBook == null)
//            {
//                MessageX.Show("日志模板打开失败");
//                return;
//            }

//            var shell = workBook.GetSheetAt(0);
//            if (shell == null)
//            {
//                MessageX.Show("工作薄打开失败");
//                workBook.Close();
//                return;
//            }

//            try
//            {
//                LastColorIndex = 0x08;
//                ICellStyle style = null;

//                ICellStyle style1 = GetStyle(workBook, Color.FromArgb(250, 191, 143));
//                ICellStyle style2 = GetStyle(workBook, Color.FromArgb(196, 215, 155));

//                double allTimer = 0;
//                var groud = dailys.GroupBy(na => na.name);
//                var rowIndex = 1;
//                int roleNumber = 0;
//                foreach (var roleGroud in groud)
//                {
//                    double roleTime = 0;
//                    string roleName = string.Empty;

//                    style = roleNumber % 2 == 0 ? style1 : style2;
//                    roleNumber++;

//                    foreach (var item in roleGroud)
//                    {
//                        roleName = item.name;
//                        var dtrow = shell.CreateRow(rowIndex);
//                        int index = 0;
//                        AddCell(dtrow, index++, item.groups, style);
//                        AddCell(dtrow, index++, item.name, style);
//                        AddCell(dtrow, index++, item.nature, style);
//                        AddCell(dtrow, index++, item.ename, style);
//                        AddCell(dtrow, index++, item.pname, style);
//                        AddCell(dtrow, index++, item.modular, style);
//                        AddCell(dtrow, index++, item.content, style);
//                        roleTime += item.workhours;
//                        AddCell(dtrow, index++, item.workhours, style);
//                        AddCell(dtrow, index++, item.back, style);
//                        AddCell(dtrow, index++, item.lctime?.ToString("yyyyMMdd"), style);
//                        rowIndex++;
//                    }

//                    allTimer += roleTime;
//                    var dtrowTime = shell.CreateRow(rowIndex);
//                    AddCell(dtrowTime, 5, roleName, style);
//                    AddCell(dtrowTime, 6, "总计工时：", style);
//                    AddCell(dtrowTime, 7, roleTime, style);
//                    rowIndex++;
//                }

//                {

//                    var dtrowTime = shell.CreateRow(rowIndex);
//                    AddCell(dtrowTime, 5, "总计所有工时：", style);
//                    AddCell(dtrowTime, 6, allTimer, style);
//                }
//                if (File.Exists(fileName))
//                    File.Delete(fileName);

//                using (FileStream fs = File.OpenWrite(fileName))
//                {
//                    workBook.Write(fs);
//                }
//            }
//            catch (Exception e)
//            {
//                Logger.Error("写入文件错误： " + fileName + "\r\n" + e.ErrorInfo());
//            }
//            finally
//            {
//                workBook.Close();
//            }
//        }
//    }
//}
