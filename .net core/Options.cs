﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExcelToUnityScript
{
    public class Options
    {

        /// <summary>
        /// 输入文件
        /// </summary>
        [Option('f', "infile", Required = true, HelpText = "输入文件。")]
        public string InFile { get; set; }
        /// <summary>
        /// 输出路径
        /// </summary>
        [Option('o', "outPath", Required = true, HelpText = "输出路径。")]
        public string OutPath { get; set; }

        /// <summary>
        /// 忽略行数
        /// </summary>
        [Option('i', "ignore", Required = false, HelpText = "忽略行数。")]
        public int IgnoreNum { get; set; } = 1;

        /// <summary>
        /// 是否向上追溯
        /// </summary>
        [Option('u', "backUp", Required = false, HelpText = "是否向上追溯。")]
        public bool IsBackUp { get; set; } = false;

        /// <summary>
        /// 继承类
        /// </summary>
        [Option('c', "class", Required = false, HelpText = "继承类。")]
        public string inheritClass { get; set; }
        /// <summary>
        /// 继承数据类
        /// </summary>
        [Option('d', "classData", Required = false, HelpText = "继承数据类。")]
        public string inheritClassData { get; set; }

    }
}
