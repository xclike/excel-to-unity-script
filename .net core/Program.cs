﻿using CommandLine;
using Excel;
using ExcelToUnityScript.UnityScript;
using System;
using System.IO;

namespace ExcelToUnityScript
{
    class Program
    {
        static void Main(string[] args)
        {

            if (null == args || args.Length == 0)
            {
                args = new string[] { "-f", "E:/Unity/FaceChange/Assets/Resources/ExcelConfig/LevelUICfg.xlsx",
                    "-o", "E:/Unity/FaceChange/Assets/Script/CSVCfg/" };

            }

            Parser.Default.ParseArguments<Options>(args).WithParsed(Run);
        }

        private static void Run(Options obj)
        {

            string inFile = obj.InFile;
            string outPath = obj.OutPath;

            string inheritClass = obj.inheritClass;
            string inheritClassData = obj.inheritClassData;


            if (string.IsNullOrEmpty(inheritClass))
                inheritClass = "CSVBaseConfig";

            if (!File.Exists(inFile))
                throw new Exception($"{inFile}  文件不存在");


            if (!Directory.Exists(outPath))
                throw new Exception($"{outPath} 输出路径不存在");


            var fileName = Path.GetFileNameWithoutExtension(inFile);

            ExcelHelper excel = new ExcelHelper(obj.InFile.ToString());
            var sheet = excel.GetSheet(0);
            var lines = excel.ExportToCVSData(sheet);
            CSVScript scripte = new CSVScript(obj.IgnoreNum, obj.IsBackUp);

            string path = scripte.CreateCSVConfigFile(lines, fileName, outPath, inheritClass, inheritClassData);
            Console.WriteLine(path);

        }
    }
}
